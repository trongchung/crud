(function ($, app) {

    var homeCls = function () {
        var vars = {};
        var ele = {};

        this.run = function () {
            this.init();
            this.bindEvents();
        };

        this.init = function () {
            ele.name = document.getElementById('name');
            ele.email = document.getElementById('email');
            ele.id = document.getElementById('id');

            vars.users = JSON.parse(localStorage.getItem('users'));
            if (!vars.users) {
                vars.users = [];
            }
        };

        this.bindEvents = function () {
            submitForm();
            addStudent()
        };

        this.resize = function () {

        };

        deleteStudent = function (i) {
            vars.users.splice(i, 1);
            localStorage.setItem('users', JSON.stringify(vars.users));
            addStudent();
        };

        var  creatStudent = function () {
            var user = {
                name: ele.name.value,
                email: ele.email.value,
                id: ele.id.value
            };
            vars.users.push(user);
            localStorage.setItem('users', JSON.stringify(vars.users));
            document.getElementById('form').reset();
        };

        var addStudent = function () {
            var studentHtml = document.getElementById('table-student');
            studentHtml.innerHTML = '';
            for (var i = 0; i < vars.users.length; i++) {
                studentHtml.innerHTML += "<tr>"
                    + "<td>" + vars.users[i].name + "</td>"
                    + "<td>" + vars.users[i].email + "</td>"
                    + "<td>" + vars.users[i].id + "</td>"
                    + "<td>"
                    + "<button type='button' class='btn mr-2 edit btn-warning' onclick=\'editStudent(" + i + ")\'>Edit</button>"
                    + "<button type='button' class='btn del btn-danger' onclick=\'deleteStudent(" + i + ")\'>Delete</button>"
                    + "</td>"
                    + "</tr>";
            }
        };

        var submitForm = function () {
            if (typeof (Storage) !== "undefined") {
                document.getElementById('form').addEventListener('submit', (e) => {
                    e.preventDefault();
                    creatStudent();
                    addStudent()
                })
            }else {
                alert('Trình duyệt của bạn không hỗ trợ Storage');
            }

        };

    };


    $(document).ready(function () {
        var homeObj = new homeCls();
        homeObj.run();

        // On resize
        $(window).resize(function () {
            homeObj.resize();
        });
    });
}(jQuery, $.app));