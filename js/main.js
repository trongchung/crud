var users = [];
if (typeof (Storage) !== "undefined") {
    document.getElementById('form').addEventListener('submit', (e) => {
        e.preventDefault();
        let name = document.getElementById('name').value;
        let email = document.getElementById('email').value;
        let id = document.getElementById('id').value;
        var atposition = email.indexOf("@");
        var dotposition = email.lastIndexOf(".");
        if (name == '' || name == null) {
            alert("Tên không được để trống");
            return false;
        } else if (email == null || atposition < 1 || dotposition < (atposition + 2) || (dotposition + 2) >= email.length) {
            alert("Emai không hợp lệ");
            return false;
        } else if (id == null || id == '') {
            alert("Id không để trống");
            return false;
        }
        creatStudent(name, email, id);
        addStudent();

    })
} else {
    alert('Trình duyệt của bạn không hỗ trợ Storage');
}


function creatStudent(name, email, id) {
    var users = JSON.parse(localStorage.getItem('qlsv'));
    if (!users) {
        users = [];
    }
    var user = {
        name: name,
        email: email,
        id: id
    }
    users.push(user);
    document.getElementById('form').reset();
    localStorage.setItem('qlsv', JSON.stringify(users));
}

function addStudent() {
    var users = JSON.parse(localStorage.getItem('qlsv'));


    if (!users) {
        users = [];
    }
    var studentHtml = document.getElementById('table-student');
    studentHtml.innerHTML = '';
    for (var i = 0; i < users.length; i++) {
        studentHtml.innerHTML += "<tr>"
            + "<td>" + users[i].name + "</td>"
            + "<td>" + users[i].email + "</td>"
            + "<td>" + users[i].id + "</td>"
            + "<td>"
            + "<button type='button' class='btn mr-2 edit btn-warning' onclick=\'editStudent(" + i + ")\'>Edit</button>"
            + "<button type='button' class='btn del btn-danger' onclick=\'deleteStudent(" + i + ")\'>Delete</button>"
            + "</td>"
            + "</tr>";
    }

}

addStudent();

function deleteStudent(i) {
    var users = JSON.parse(localStorage.getItem('qlsv'));
    users.splice(i, 1);
    localStorage.setItem('qlsv', JSON.stringify(users));
    addStudent();
}

function editStudent(index) {
    var studentHtml = document.getElementById('table-student');
    var users = JSON.parse(localStorage.getItem('qlsv'));
    studentHtml.innerHTML = '';
    for (var i = 0; i < users.length; i++) {
        if (i == index) {
            studentHtml.innerHTML += "<tr>"
                + "<td>"
                + "<input  type='text' id='edit_name' value='" + users[i].name + "' class='form-control'/>"
                + "</td>"
                + "<td>"
                + "<input  type='text' id='edit_email' value='" + users[i].email + "' class='form-control'/>"
                + "</td>"
                + "<td>"
                + "<input  type='text' id='edit_id' value='" + users[i].id + "' class='form-control'/>"
                + "</td>"
                + "<td>"
                + "<button type='button' class='btn mr-2 edit btn-warning' onclick=\'updateStudent(" + i + ")\'>Update</button>"
                + "<button type='button' class='btn del btn-danger' onclick=\'addStudent(" + i + ")\'>Cancel</button>"
                + "</td>"
                + "</tr>";
            localStorage.setItem('qlsv', JSON.stringify(users));
        } else {
            studentHtml.innerHTML += "<tr>"
                + "<td>" + users[i].name + "</td>"
                + "<td>" + users[i].email + "</td>"
                + "<td>" + users[i].id + "</td>"
                + "<td>"
                + "<button type='button' disabled class='btn mr-2 edit btn-warning'>Edit</button>"
                + "<button type='button' disabled class='btn del btn-danger'>Delete</button>"
                + "</td>"
                + "</tr>";
        }


    }


}

function updateStudent(index) {
    var users = JSON.parse(localStorage.getItem('qlsv'));
    var edit_name = document.getElementById('edit_name').value;
    var edit_email = document.getElementById('edit_email').value;
    var edit_id = document.getElementById('edit_id').value;

    if (edit_name == '' || edit_email == '' || edit_id == '') {
        alert('INIT');
    } else {
        users[index].name = edit_name;
        users[index].email = edit_email;
        users[index].id = edit_id;
    }
    localStorage.setItem('qlsv', JSON.stringify(users));
    addStudent();

}


function searchStudent() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("searchInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("table-student");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}




